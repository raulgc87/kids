import coche from './assets/coche.svg'

const ConfigClient = {
   logo: coche,
   mainColor: '#3072D3'
};

export default ConfigClient;