import React, { useState, useEffect } from "react";

const userID = () => {
  //GET DATA
  const [userState, getUserState] = useState(null);

  useEffect(() => {
    const options = {
        method: 'GET',
        headers: {
            Authorization: 'Bearer 876fa191cc975c551c55b70468e4e9a6f6a980f5d54befbc828d963b8e367ca2'
        }
    };
    fetch("http://localhost:3210/getUser/0", options)
      .then((response) => response.json())
      .then((u) => {
        getUserState(u.data);
    });
  }, []);

    console.log('HOLA', userState);
    
    return  userState;
};

export default userID;


//const IDuser = userID();
//console.log(IDuser && console.log("USUARIO: ", IDuser));