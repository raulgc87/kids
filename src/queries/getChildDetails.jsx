import React, { useState, useEffect } from "react";

const childDetails = () => {
  //GET DATA
  const [childDetailState, getChildDetailState] = useState(null);

  useEffect(() => {
    const options = {
        method: 'GET',
        headers: {
            Authorization: 'Bearer 876fa191cc975c551c55b70468e4e9a6f6a980f5d54befbc828d963b8e367ca2'
        }
    };
    fetch("http://localhost:3210/getChildDetails/0", options)
      .then((response) => response.json())
      .then((c) => {
        getChildDetailState(c.data);
    });
  }, []);

    console.log('HOLA', childDetailState);
    
    return  childDetailState;
};

export default childDetails;


//const IDuser = userID();
//console.log(IDuser && console.log("USUARIO: ", IDuser));