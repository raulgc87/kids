import React from "react";
import { Route } from "wouter";

import Home from './childList/index'
import ChildHistory from './childDetail/historyCharts/index'
import ChildDetail from './childDetail/simpleDay/index'

const ContentApp = () => {
  return (
    <>
      <Route path="/" component={Home} />
      <Route path="/child-history" component={ChildHistory} />
      <Route path="/child-day" component={ChildDetail} />
    </>
  );
}

export default ContentApp;