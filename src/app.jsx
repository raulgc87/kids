import React from 'react';
import ContentApp from './router'

const App = () => {
  return (
      <ContentApp />
  );
}

export default App;