import React from 'react';
import styled from 'styled-components';
import ChildList from './childCard';
import ConfigClient from '../config';

let name = 'Raul';

const NameWelcome = styled.h1`
    font-size: 30px;
    margin-top: 75px;
    margin-bottom: 24px;
    padding: 0;
    strong{
        font-weight: 900;
    }
`;

const CustomH2 = styled.h2`
    font-size: 18px;
    font-weight: 900;
    padding: 0;
    margin-bottom: 10px;
`;

const Logo = styled.div`
    color: #f28237;
    padding: 12px 20px;
    position: absolute;
    background: #fff;
    z-index: 10;
    img {
        height: 45px;
        width: auto;
    }
`;

const Home = () => {
    return (
    <>
        <Logo>
            <img src={ConfigClient.logo} />
        </Logo>
        <div className="content">
            <NameWelcome>¡Hola <strong>{name}</strong>!</NameWelcome>
            <CustomH2>Tus niñ@s</CustomH2>
            <ChildList />
        </div>
    </>
    );
}

export default Home;