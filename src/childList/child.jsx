import React from 'react';
import { Link } from "wouter";
import styled from 'styled-components';

const Child = ({mykey, name, surname, birthday, teacher, group, image}) => {
    //console.log(mykey);
    
    const SimpleChild = styled.li`
        border: 1px solid #f3f3f3;
        border-radius: 4px;
        list-style: none;
        margin-bottom: 20px;
        .childData{
            padding: 8px ;
            display: flex;
            .image {
                width: 100px;
                height: 100px;
                overflow: hidden;
                border-radius: 4px;
                img {
                    width: 100%;
                    height: 100%;
                    object-fit: cover;
                }
            }
            .data {
                padding: 0 16px;
                width: calc(100% - 100px);
                h3 {
                    margin: 0;
                    font-size: 18px;
                    font-weight: 900;
                }
                .birthday {
                    font-size: 10px;
                    display: block;
                    margin-bottom: 14px;
                    opacity: 0.5;
                    letter-spacing: 1px;
                }
                .dataList {
                    display: flex;
                    justify-content: space-between;
                    font-size: 14px;
                    margin-bottom: 4px;
                }
            }
        }
        .actions{
            display: flex;
            justify-content: flex-end;
            border-top: 1px solid #f3f3f3;
            span{
                font-size: 12px;
                font-weight: bold;
                letter-spacing: 1px;
                padding: 4px 10px;
                margin: 10px;
                color: #3275D5;
                background: rgba(48, 114, 211, 0.1);
                border-radius: 8px;
                &.today{
                    background: #3275D5;
                    color: #fff;
                }
            }
        }
    `;
    
    return(
        <SimpleChild>
            <div className="childData">
                <div className="image">
                    <img src={image}></img>
                </div>
                <div className='data'>
                    <h3>{name} {surname}</h3>
                    <div className='birthday'>{birthday}</div>
                    <div className='dataList'>
                        <strong>Tutor</strong>
                        <span>{teacher}</span>
                    </div>
                    <div className='dataList'>
                        <strong>Grupo</strong>
                        <span>{group}</span>
                    </div>
                </div>
            </div>
            <div className="actions">
                <Link href="/child-history"><span>Historial</span></Link>
                <span>Calendario</span>
                <Link href="/child-day"><span className='today'>Hoy</span></Link>
            </div>
        </SimpleChild>
    );
}

export default Child;