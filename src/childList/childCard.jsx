import React from 'react';
import styled from 'styled-components';
import Child from './child';
import childDetails from '../queries/getChildDetails';

//FAKE DATA
let image = 'https://images.unsplash.com/photo-1491013516836-7db643ee125a?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=2825&q=80'

const CustomUL = styled.ul`
    padding: 0;
    margin: 0;
`;


const ChildList = () => {

    const childData = childDetails();
    console.log(childData && console.log("child: ", childData));

    return (
        <CustomUL>
            {childData?.map((child, index) => (
                <Child 
                    key={index}
                    mykey={index}
                    name={child.name} 
                    surname={child.surname}
                    birthday={child.birthday}
                    teacher={child.teacher}
                    group={child.idClass}
                    image={image}
                />
            ))}
        </CustomUL>
    );
}

export default ChildList;