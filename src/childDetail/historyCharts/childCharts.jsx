import React from 'react';
import ChartLine from '../common/charts';
import styled from 'styled-components';

const ChartTitle = styled.h2`
    font-size: 24px;
    font-weight: 900;
`;

const Card = styled.div`
    z-index: 10;
    position: relative;
    background: #fff;
    border-radius: 32px 32px 0 0;
    padding: 24px;
    margin-top: -40px;
    h1 {    
        margin-top: 0;
        font-weight: 300;
    }
`;

const ChildCharts = () => {
    
    const shit = [
        {
          name: 'Page A',
          uv: 1,
        },
        {
          name: 'Page B',
          uv: 2,
        },
        {
          name: 'Page C',
          uv: 0,
        },
        {
          name: 'Page D',
          uv: 3,
        },
        {
          name: 'Page E',
          uv: 1,
        },
        {
          name: 'Page F',
          uv: 1,
        },
        {
          name: 'Page G',
          uv: 2,
        },
      ];

    return (
      <Card>
          <h1>Estadísticas</h1>
          <section>
              <ChartTitle>Comida</ChartTitle>
              <ChartLine title="Desayuno" data={shit} dataKey="uv" strokeColor="#3072D3" />
              <ChartLine title="Comida" data={shit} dataKey="uv" strokeColor="#3072D3" />
              <ChartLine title="Merienda" data={shit} dataKey="uv" strokeColor="#3072D3" />
          </section>
          <section>
              <ChartTitle>Cacas</ChartTitle>
              <ChartLine title="Mañana" data={shit} dataKey="uv" strokeColor="#3072D3" />
              <ChartLine title="Tarde" data={shit} dataKey="uv" strokeColor="#3072D3" />
          </section>
          <section>
              <ChartTitle>Sueño</ChartTitle>
              <ChartLine title="Mañana" data={shit} dataKey="uv" strokeColor="#3072D3" />
              <ChartLine title="Tarde" data={shit} dataKey="uv" strokeColor="#3072D3" />
          </section>
      </Card>
    );
}

export default ChildCharts;