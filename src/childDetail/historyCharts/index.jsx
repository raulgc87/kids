import React from 'react';
import Hero from '../common/hero';
import ChildCharts from './childCharts';

const ChildHistory = () => {
    return (
    <>
        <Hero childname="Raul" childSurname="Garcia Castilla" age ="1-2"/>
        <ChildCharts />
    </>
    );
}

export default ChildHistory;