import React from 'react';
import styled from 'styled-components';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import MoreVertIcon from '@mui/icons-material/MoreVert';

const MenuChild = () => {
    const [anchorEl, setAnchorEl] = React.useState(null);
    const open = Boolean(anchorEl);
    const handleClick = (event) => { setAnchorEl(event.currentTarget); };
    const handleClose = () => { setAnchorEl(null); };

    return (
        <>
            <MoreVertIcon 
                aria-controls={open ? 'menuchild-button' : undefined}
                aria-haspopup="true"
                aria-expanded={open ? 'true' : undefined}
                onClick={handleClick}
            />
            <Menu
                id="menuchild-button"
                anchorEl={anchorEl}
                autoFocus={false}
                open={open}
                onClose={handleClose}
                MenuListProps={{
                    'aria-labelledby': 'basic-button',
                }}
                >
                <MenuItem onClick={handleClose}>Cambiar imagen</MenuItem>
                <MenuItem onClick={handleClose}>Hoy</MenuItem>
                <MenuItem onClick={handleClose}>Calendario</MenuItem>
                <MenuItem onClick={handleClose}>Estadísticas</MenuItem>
            </Menu>
        </>
    )
};

export default MenuChild;