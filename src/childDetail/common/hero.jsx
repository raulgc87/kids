import React from 'react';
import { Link } from "wouter";
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import MenuChild from './menuChild';
import styled from 'styled-components';

const ImageHeroContent = styled.div `
    background: url('https://images.unsplash.com/photo-1491013516836-7db643ee125a?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=2825&q=80') no-repeat center center;
    background-size: cover;
    position: sticky;
    top: 0;
    height: 390px;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    overflow: hidden;
    .bottom-data{
        padding: 24px 24px 40px 24px;
        display: flex;
        justify-content: space-between;
        h1 {
            color: #fff;
            font-weight: 300;
            line-height: 1em;
            text-shadow: 0px 0px 10px #00000070;
            span{
                display: block;
                font-weight: 900;
            }
        }
        .age {
            background: #3072D3;
            border-radius: 5em;
            text-align: center;
            color: #fff;
            font-size: 32px;
            font-weight: 900;
            width: 90px;
            height: 90px;
            display: flex;
            flex-direction: column;
            justify-content: center;
            line-height: 1em;
            span {
                font-size: 0.7em;
                display: block;
                font-weight: 400;
            }
        }
    }
`;

const CustomHeader = styled.div`
    display: flex;
    justify-content: space-between;
    padding: 20px 12px 20px 20px;
    svg {
        font-size: 30px;
        &:first-child{
            stroke: white;
            stroke-width: 2px;
        }
    }
    &:before{
        content: '';
        width: 80px;
        height: 80px;
        background: #fff;
        position: absolute;
        top: -12px;
        right: -16px;
        border-radius: 5em;
        z-index: -2;
    }
    
`;

const Hero = (props) => {
    return(
        <>  
            <ImageHeroContent>
                <CustomHeader>
                    <Link href="/"><ArrowBackIcon /></Link>
                    <MenuChild />
                </CustomHeader>
                <div className="bottom-data">
                    <h1><span>{props.childname}</span> {props.childSurname}</h1>
                    <div className='age'>
                        {props.age}
                        <span>anys</span>
                    </div>
                </div>
            </ImageHeroContent>
        </>
    );
};

export default Hero;