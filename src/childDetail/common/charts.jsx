import React from 'react';
import { AreaChart, Area, CartesianGrid, Tooltip, ResponsiveContainer} from 'recharts';
import styled from 'styled-components';

const Content = styled.div`
    border: 1px solid #F3F3F3;
    border-radius: 4px;
    padding: 12px;
    margin-bottom: 24px;
    width: 100%;
    height: 120px;
`;

const Title = styled.h3`
    font-size: 16px;
    font-weight: 400;
    margin: 0 0 10px 0;
    padding: 0;
`;

const ChartLine = (props) => (
    <Content>
        <Title>{props.title}</Title>
        <ResponsiveContainer width="100%" height="70%">
        <AreaChart width={500} height={70} data={props.data}>
            <CartesianGrid strokeDasharray="2 3" />
            <Tooltip />            
            <Area 
                type="monotone" 
                dataKey={props.dataKey} 
                stroke={props.strokeColor}
                fill={props.strokeColor}
                fillOpacity={0.2}
                strokeWidth={4}
                dot={0}
            />
        </AreaChart>
        </ResponsiveContainer>
    </Content>
);

export default ChartLine;