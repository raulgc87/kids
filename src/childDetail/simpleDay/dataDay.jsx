import React from 'react';
import styled from 'styled-components';
import Food from './food';
import Shit from './shit';
import Sleeping from './sleeping';

const Card = styled.div`
    z-index: 10;
    position: relative;
    background: #fff;
    border-radius: 32px 32px 0 0;
    padding: 24px;
    margin-top: -40px;
    h1 {    
        margin-top: 0;
        font-weight: 900;
        span {
            font-weight: 300;
            display: block;
        }
    }
    h2{
        font-size: 24px;
        font-weight: 900;
        margin: 32px 0 0 0;
    }
`;


const DataDay = () => {
    
    const dataDay = {
        day : '14 de Abril de 2022',
        food: {
            breakfast: 4,
            lunch: 3,
            appetizer: 1
        },  
        shit : {
            morning: 1,
            afternoon: 1
        }, 
        sleeping : {
            morning: 2,
            morningTime: ['10:00', '12:00'],
            afternoon: 2,
            afternoonTime: ['17:00', '18:00']
        }
    };

    console.log(dataDay);

    return (
      <Card>
            <h1>Hoy dia <span>{dataDay.day}</span></h1>
            <h2>Comida</h2>
            <Food
                title='Desayuno'
                mark={dataDay.food.breakfast}
            />
            <Food
                title='Comida'
                mark={dataDay.food.lunch}
            />
            <Food
                title='Merienda'
                mark={dataDay.food.appetizer}
            />
            <h2>Shit</h2>
            <Shit
                title='Mañana'
                morningMark={dataDay.shit.morning}
                afternoonMark={dataDay.shit.afternoon}
            />
            <h2>Sueño</h2>
            <Sleeping
                title='Mañana'
                morningMark={dataDay.sleeping.morning}
                morningTime={dataDay.sleeping.morningTime[0] + ` a ` + dataDay.sleeping.morningTime[1]}
                afternoonMark={dataDay.sleeping.afternoon}
                afternoonTime={dataDay.sleeping.afternoonTime[0] + ` a ` + dataDay.sleeping.afternoonTime[1]}
            />
      </Card>
    );
}

export default DataDay;