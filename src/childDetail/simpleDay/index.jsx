import React from 'react';
import Hero from '../common/hero';
import DataDay from './dataDay';

const ChildDetail = () => {
    return (
    <>
        <Hero childname="Raul" childSurname="Garcia Castilla" age ="1-2"/>
        <DataDay />
    </>
    );
}

export default ChildDetail;