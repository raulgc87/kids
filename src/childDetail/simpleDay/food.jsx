import React from 'react';
import styled from 'styled-components';

//IMPORT EMOJIS
import cry from '../../assets/emojis/cry.svg';
import sad from '../../assets/emojis/sad.svg';
import happy from '../../assets/emojis/happy.svg';
import smile from '../../assets/emojis/smile.svg';

const Section = styled.div`
    border: 1px solid #f3f3f5;
    border-radius: 8px;
    padding: 10px;
    margin-bottom: 8px;
    h3{
        margin: 0;
        color: #3072D3;
        font-size: 16px;
        font-weight: 300;
    }
    ul {
        padding: 0;
        margin: 0;
        list-style: none;
        display: flex;
        justify-content: space-between;
        li {
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: space-between;
            &.active {
                opacity: 1;
            }
            &.disabled {
                opacity: 0.2;
            }
            span {
                color: #3072D3;
                font-weight: 900;
                font-size: 12px;
            }
            img {
                max-width: 75px;
            }
        }
    }
`;

const Food = (props) => {
    return (
        <Section>
            <h3>{props.title}</h3>
            <ul className='emoji-list'>
                <li className={props.mark === 1 ? 'active' : 'disabled'} >
                    <img src={cry} />
                    <span>Gens</span>
                </li>
                <li className={props.mark === 2 ? 'active' : 'disabled'} >
                    <img src={sad} />
                    <span>Poc</span>
                </li>
                <li className={props.mark === 3 ? 'active' : 'disabled'}>
                    <img src={happy} />
                    <span>Bastant</span>
                </li>
                <li className={props.mark === 4 ? 'active' : 'disabled'}>
                    <img src={smile} />
                    <span>Tot</span>
                </li>
            </ul>
        </Section>
    );
}

export default Food;