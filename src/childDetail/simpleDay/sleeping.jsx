import React from 'react';
import styled from 'styled-components';

//IMPORT EMOJIS
import cryLot from '../../assets/emojis/cryLot.svg';
import sleeping from '../../assets/emojis/sleeping.svg';

const Section = styled.div`
    display: flex;
    justify-content: space-between;
    .half {
        border: 1px solid #f3f3f5;
        border-radius: 8px;
        padding: 10px;
        max-width: 49%;
        width: 100%;
        h3{
            margin: 0;
            color: #3072D3;
            font-size: 16px;
            font-weight: 300;
        }
        ul {
            padding: 0;
            margin: 0;
            list-style: none;
            display: flex;
            justify-content: space-between;
            li {
                display: flex;
                flex-direction: column;
                align-items: center;
                justify-content: space-between;
                &.active {
                    opacity: 1;
                }
                &.disabled {
                    opacity: 0.2;
                }
                span {
                    color: #3072D3;
                    font-weight: 900;
                    font-size: 12px;
                }
                img {
                    max-width: 75px;
                }
            }
        }
        .time {
            font-size: 22px;
            color: #3072D3;
            width: 100%;
            text-align: center;
            margin-top: 20px;
        }
    }
`;

const Sleeping = (props) => {
    return (
        <Section>
            <div className='half'>
                <h3>{props.title}</h3>
                <ul className='emoji-list'>
                    <li className={props.morningMark === 1 ? 'active' : 'disabled'} >
                        <img src={cryLot} />
                        <span>Mal</span>
                    </li>
                    <li className={props.morningMark === 2 ? 'active' : 'disabled'} >
                        <img src={sleeping} />
                        <span>Bien</span>
                    </li>
                </ul>
                <div className='time'>{props.morningTime}</div>
            </div>
            <div className='half'>
                <h3>{props.title}</h3>
                <ul className='emoji-list'>
                    <li className={props.afternoonMark === 1 ? 'active' : 'disabled'} >
                        <img src={cryLot} />
                        <span>Mal</span>
                    </li>
                    <li className={props.afternoonMark === 2 ? 'active' : 'disabled'} >
                        <img src={sleeping} />
                        <span>Bien</span>
                    </li>
                </ul>
                <div className='time'>{props.afternoonTime}</div>
            </div>
        </Section>
    );
}

export default Sleeping;